import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) { }
  public actionListInforation = ['Voter Disposition', 'Voter Profile', 'Workers Activity', 'Top Issues', 'Social Media Mgt', 'Voting Day'];
  ngOnInit() {
  }

  logOutUser(){
    this.router.navigateByUrl('/login');    

  }
}
